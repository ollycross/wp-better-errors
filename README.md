# Better Errors

Wordpress error handling using `WP_DEBUG` is a little unsophisticated.  Setting it to true will set `error_reporting` to
`E_ALL` and setting it to `false` will set it to a specific list.  Unfortunately due to the order in which things
load in Wordpress it is not possible to persist changes to `error_reporting` in `wp-config.php`.  This can often cause 
issues on staging environments, where we want to display some errors, but disable errors level such as
`E_DEPERCATED` or `E_STRICT`.

Enter `better-errors`, an `mu-plugin` which sits between Wordpress setting `error_reporting` and related settings and
Wordpress loading plugins (the main culprits of deprecation errors).

Better errors listens to the `WP_ENV` constant to decide what to do.  It has some recommended defaults for
`development`, `staging` and `production` environments.  These can be overridden by setting further constants:
- `WP_ERROR_REPORTING` will be passed to `error_reporting(...)`
- `WP_DISPLAY_ERRORS` will be passed to `ini_set('display_errors', ...)`
- `WP_LOG_ERRORS` will be passed to `ini_set('log_errors', ...)`
- `WP_ERROR_LOG` will be passed to `ini_set('error_log', ...)`

Setting these constants using environmental values will allow you to fine-tune settings per-environment.

 `WP_ERROR_REPORTING` takes a bitmask value.  If setting as an environmental value the following tool may be useful:
 https://maximivanov.github.io/php-error-reporting-calculator/