<?php
/**
 * Plugin Name: Better Errors
 * Description: Better error handling for Wordpress projects
 * Author:      Olly Cross <olly@ollyollyolly.com>
 * License:     GNU General Public License v3 or later
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * Version: 1.0.1
 */

$env = defined('WP_ENV') ? WP_ENV : 'production';

switch ($env) {
    case 'development':
    case 'dev':
        /*
         * Development
         * Defaults:
         * - Display all errors;
         * - Log all errors to server log
         */
        ini_set(
            'display_errors',
            defined('WP_DISPLAY_ERRORS') ? WP_DISPLAY_ERRORS : true
        );
        ini_set(
            'log_errors',
            defined('WP_LOG_ERRORS') ? WP_LOG_ERRORS : 0
        );
        ini_set(
            'error_log',
            defined('WP_ERROR_LOG') ? WP_ERROR_LOG : null
        );

        error_reporting(
            defined('WP_ERROR_REPORTING') ?
                WP_ERROR_REPORTING : E_ALL
        );
        define('QM_DISABLE_ERROR_HANDLER', true);
        break;

    case 'stage':
    case 'staging':
        /*
         * Staging
         * - Display all errors EXCEPT deprecation and strict error
         * - Log all errors EXCEPT deprecation and strict error to server log
         */
        ini_set(
            'display_errors',
            defined('WP_DISPLAY_ERRORS') ? WP_DISPLAY_ERRORS : true
        );
        ini_set(
            'log_errors',
            defined('WP_LOG_ERRORS') ? WP_LOG_ERRORS : 0
        );
        ini_set(
            'error_log',
            defined('WP_ERROR_LOG') ? WP_ERROR_LOG : null
        );

        error_reporting(
            defined('WP_ERROR_REPORTING') ?
                WP_ERROR_REPORTING : E_ALL & ~E_DEPRECATED & ~E_STRICT
        );
        break;

    case 'production':
    case 'live':
    default:
        /*
         * Production
         * - Do not display errors
         * - Log all errors EXCEPT deprecation and strict error to server log
         */
        ini_set(
            'display_errors',
            defined('WP_DISPLAY_ERRORS') ? WP_DISPLAY_ERRORS : false
        );
        ini_set(
            'log_errors',
            defined('WP_LOG_ERRORS') ? WP_LOG_ERRORS : 0
        );
        ini_set(
            'error_log',
            defined('WP_ERROR_LOG') ? WP_ERROR_LOG : null
        );

        error_reporting(
            defined('WP_ERROR_REPORTING') ?
                WP_ERROR_REPORTING : E_ALL & ~E_DEPRECATED & ~E_USER_DEPRECATED & ~E_STRICT
        );
        break;
}

